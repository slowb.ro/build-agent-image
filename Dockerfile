FROM nikolaik/python-nodejs:python3.11-nodejs20

RUN \
 install -m 0755 -d /etc/apt/keyrings &&\
 curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg &&\
 chmod a+r /etc/apt/keyrings/docker.gpg &&\
 echo \
   "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" > /etc/apt/sources.list.d/docker.list && \
apt-get update && \
apt-get install docker-ce -y && \
rm -rf /var/lib/apt/lists/*

#RUN usermod -aG docker pn

USER root
WORKDIR /home/pn/app
